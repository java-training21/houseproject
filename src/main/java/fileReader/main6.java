package fileReader;

import org.springframework.boot.CommandLineRunner;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class main6 implements CommandLineRunner {

    public static void main(String[] args) throws FileNotFoundException { //dodano wyjatek Scanner(file)
        File file = new File("plik01.txt");
        Scanner scanner = new Scanner(file);

        String absolutePath = file.getAbsolutePath();
        System.out.println("Absolutna sciezka pliku to: " + absolutePath);
        boolean exists = file.exists();
        System.out.println("Czy plik istnieje: " + exists);
        boolean isFile = file.isFile();
        System.out.println("Czy plik jest plikiem: " + isFile);
        String myFileName = file.getName();
        System.out.println("Nazwa mojego pliku to: " + myFileName);
        boolean isRead = file.canRead();
        System.out.println("Czy plik jest do odczytu: "+ isRead);
        boolean isWrite = file.canWrite();
        System.out.println("Czy plik jest do zapisu: "+ isWrite);
        long lasModified = file.lastModified();
        System.out.println("Ostatnio modyfikowany: " + lasModified);

        System.out.println("####################   Odczyt z pliku:");
        while(scanner.hasNext()){
            String line = scanner.nextLine();
            System.out.println(line);
        }
        //String linia1 = scanner.nextLine();
        //System.out.println(linia1);
        //String linia2 = scanner.nextLine();
        //System.out.println(linia2);
    }
    @Override
    public void run(String... args) throws Exception {

    }
}
