package pl.houseProject_Spring.house;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Strych {

    private Pietro pietro;

    @Autowired
    public Strych(Pietro pietro) {
        this.pietro = pietro;
    }

    public void printStrych() {
        House house = new House();
        //Pietro pietro = new Pietro(); //wstrzykiwanie zaleznosci
        pietro.printPietro();
        house.printHouseMaxLevel("strych");
    }
}
