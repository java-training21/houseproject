package pl.houseProject_Spring.house;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Pietro {

    public Parter parter;
    //public Piwnica piwnica;

    @Autowired
    public Pietro(Parter parter) {
        this.parter = parter;
    }


    public void printPietro() {
        House house = new House();
        //Parter parter = new Parter(); //wstrzykiwanie zaleznosci
        parter.printParter();
        house.printHouseMaxLevel("Pietro");


    }
}
