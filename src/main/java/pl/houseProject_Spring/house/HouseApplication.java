package pl.houseProject_Spring.house;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

@SpringBootApplication
public class HouseApplication implements CommandLineRunner {

	@Autowired
	Piwnica piwnica;
	@Autowired
	Parter parter;
	@Autowired
	Pietro pietro;
	@Autowired
	Strych strych;

	public static void main(String[] args) {
		SpringApplication.run(HouseApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		//piwnica.printPiwnica();
		//parter.printParter();
		//pietro.printPietro();
		strych.printStrych();

		 //klasa Scanner - wczytywanie z klawiatury i drukowanie na konsoli
		System.out.println("Username: ");
		Scanner scanner = new Scanner(System.in);
		String username = scanner.nextLine();
		System.out.println("Username is: " + username);
		while(1==1) {
			System.out.println("Give first number: ");
			int numberOne = scanner.nextInt();
			System.out.println("Give second number: ");
			int numberTwo = scanner.nextInt();
			if (numberOne > numberTwo)
				System.out.println("First number is bigger than second.");
			else if (numberOne < numberTwo)
				System.out.println("First number is smaller than second.");
			else
				System.out.println("Both number are equal");
			System.out.println("One more time! Try again!");
		}




	}
}
