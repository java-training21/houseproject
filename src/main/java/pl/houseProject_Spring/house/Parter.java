package pl.houseProject_Spring.house;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Parter {

    private Piwnica piwnica;

    @Autowired
    public Parter(Piwnica piwnica) {
        this.piwnica = piwnica;
    }

    public void printParter() {
        //Piwnica piwnica = new Piwnica(); //wstrzykiwanie zaleznosci
        House house = new House();
        piwnica.printPiwnica();
        house.printHouseMaxLevel("parter");

    }
}
