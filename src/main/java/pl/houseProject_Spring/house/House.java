package pl.houseProject_Spring.house;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class House {
    @Value("piwnica")
    private String kondygnacja_0;
    @Value("parter")
    private String kondygnacja_1;
    @Value("Pierwsze pietro")
    private String kondygancja_2;

    /*
    public House(String kondygnacja_0, String kondygnacja_1, String kondygancja_2) {
        this.kondygnacja_0 = kondygnacja_0;
        this.kondygnacja_1 = kondygnacja_1;
        this.kondygancja_2 = kondygancja_2;
    }
    */


    public void printHouseMaxLevel(String kondygnacjaMax) {
        System.out.println("Budynek składa sie z: " + kondygnacjaMax); //
    }


}
