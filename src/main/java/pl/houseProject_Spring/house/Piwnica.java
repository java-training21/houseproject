package pl.houseProject_Spring.house;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Piwnica {
    private House house;

    @Autowired
    public Piwnica(House house) {
        this.house = house;
    }

    public void printPiwnica() {
        //House house = new House("kond0", "kond1", "kond2");    //rozwiazanie przez wstrzykiwanie zaleznosci (utworzenie nowego pola klasy i konstruktora)
        house.printHouseMaxLevel("piwnica");

    }




}
