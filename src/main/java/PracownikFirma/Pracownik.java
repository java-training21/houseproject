package PracownikFirma;
import org.springframework.stereotype.Component;

@Component
public class Pracownik {

    private String name;
    private String surname;
    private int age;



    public String getName() {
        return name;
    }
    public String getSurname() {
        return surname;
    }
    public int getAge() {
        return age;
    }
    public void setName(String name) {
        this.name = name;
        System.out.println("Imie pracownika to: " + name);
    }
    public void setSurname(String surname) {
        this.surname = surname;
        System.out.println("Nazwisko pracownika to: " + surname);
    }
    public void setAge(int age) {
        this.age = age;
        System.out.println("Wiek pracownika to: " + age);
    }


}
