package PracownikFirma;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.houseProject_Spring.house.Piwnica;

import java.util.Scanner;

@SpringBootApplication
public class main implements CommandLineRunner {

    @Autowired
    Firma firma;

    public static void main(String[] args) {
        SpringApplication.run(main.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        //System.out.println("Username: ");
        firma.printFirma();

    }
}

