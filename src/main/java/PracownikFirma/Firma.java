package PracownikFirma;

import org.springframework.stereotype.Component;
import java.util.Scanner;

@Component
public class Firma {

    public void printFirma() {
        //Pracownik pracownik1 = new Pracownik();
        Pracownik[] pracownicy = new Pracownik[3];

        String[] imie = new String[3];
        String[] nazwisko = new String[3];
        int[] wiek = new int[3];

        for(int i =0; i<3; i++){
            Scanner scanner = new Scanner(System.in);
            //pracownicy[i] = new Pracownik();
            System.out.println("Podaj imie pracownika nr: " + i + " ");
            imie[i] = scanner.nextLine();
            //pracownicy[i].setName(imie[i]);

            System.out.println("Podaj nazwisko pracownika nr: " + i + " ");
            nazwisko[i] = scanner.nextLine();
            //pracownicy[i].setSurname(nazwisko[i]);

            System.out.println("Podaj wiek pracownika nr: " + i + " ");
            wiek[i] = scanner.nextInt();
            //pracownicy[i].setAge(wiek[i]);

            //pracownicy[i] = new Pracownik();
            //pracownicy[i].setName(imie[i]);
            //pracownicy[i].setSurname(nazwisko[i]);
            //pracownicy[i].setAge(wiek[i]);
        }
        for(int j=0;j<3;j++) {
            System.out.println("------ Dane pracownika ["+j+"]: ");
            pracownicy[j] = new Pracownik();
            pracownicy[j].setName(imie[j]);
            pracownicy[j].setSurname(nazwisko[j]);
            pracownicy[j].setAge(wiek[j]);
        }


        //Rozwiązanie opcjonalne bez tablic:
        /*
        System.out.println("Podaj imie pracownika: ");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj imie: ");
        String imie = scanner.nextLine();

        System.out.println("Podaj nazwisko: ");
        String nazwisko = scanner.nextLine();

        System.out.println("Podaj wiek: ");
        int wiek = scanner.nextInt();

        pracownik1.setName(imie);
        pracownik1.setSurname(nazwisko);
        pracownik1.setAge(wiek);
         */


    }


}
