package tablicaZmiennyRozmiar;

import org.springframework.boot.CommandLineRunner;

import java.util.Scanner;

public class main4 implements CommandLineRunner {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj rozmiar tablicy: ");
        int n = scanner.nextInt();
        int[] tab = new int[n];

        for(int i = 0;i<n;i++) {
            //int[] tab = new int[n];
            System.out.println("Podaj element [" + i + "]: ");
            tab[i] = scanner.nextInt();
        }
        int j = 0;
        while(j<n){
            System.out.println("Podany element tablicy nr [" + "]: " + tab[j]);
            j++;
        }
    }

    @Override
    public void run(String... args) throws Exception {

    }
}
